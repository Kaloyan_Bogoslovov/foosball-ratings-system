package bogoslovov.kaloyan.foosballratingssystem.utils;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import bogoslovov.kaloyan.foosballratingssystem.data.input.GameScore;


public class ActivityUtils {

    public static void addFragmentToActivity (@NonNull FragmentManager fragmentManager,
                                              @NonNull Fragment fragment, int frameId) {

        Utils.checkNotNull(fragmentManager);
        Utils.checkNotNull(fragment);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.commit();
    }

    public static List<GameScore> readTextFromUri(Context context, Uri uri) throws IOException {
        InputStream inputStream = context.getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        List<GameScore> gameScoreList = new ArrayList<>();
        String line;

        reader.readLine();
        while ((line = reader.readLine()) != null) {
            line = line.replace("\"", "");
            String [] score = line.split(",");
            if (isDataValid(score)){
                gameScoreList.add(new GameScore(
                        score[0],
                        Integer.parseInt(score[1]),
                        score[2],
                        Integer.parseInt(score[3]))
                );
            }
        }
        inputStream.close();
        return gameScoreList;
    }

    private static boolean isDataValid(String [] score){
        if (score[0].isEmpty()){
            return false;
        } else if (score[1].isEmpty() || !score[1].matches(".*\\d+.*")){
            return false;
        } else if (score[2].isEmpty()){
            return false;
        } else if (score[3].isEmpty() || !score[3].matches(".*\\d+.*")){
            return false;
        }
        return true;
    }


}
