package bogoslovov.kaloyan.foosballratingssystem.common;

import android.app.Application;

import org.greenrobot.greendao.database.Database;

import bogoslovov.kaloyan.foosballratingssystem.data.db.DaoMaster;
import bogoslovov.kaloyan.foosballratingssystem.data.db.DaoSession;

public class FoosballRatingsBaseApp extends Application {
    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "foosball-ratings-db");
        Database db = helper.getWritableDb();
        mDaoSession = new DaoMaster(db).newSession();
    }

    public DaoSession getDaoSession(){
        return mDaoSession;
    }

    public static class Permissions {
        public static boolean localLog = true;
    }
}
