package bogoslovov.kaloyan.foosballratingssystem.data;

import org.greenrobot.greendao.query.Query;

import java.util.List;
import java.util.Map;

import bogoslovov.kaloyan.foosballratingssystem.data.db.DaoSession;
import bogoslovov.kaloyan.foosballratingssystem.data.db.Player;
import bogoslovov.kaloyan.foosballratingssystem.data.db.PlayerDao;

/**
 * Contains all methods that access the database
 */
public class RatingsRepository implements RatingsDataSource{
    private static RatingsRepository INSTANCE = null;

    private DaoSession mDaoSession;

    private RatingsRepository(DaoSession daoSession) {
        mDaoSession = daoSession;
    }

    public static RatingsRepository getInstance(DaoSession daoSession) {
        if (INSTANCE == null) {
            INSTANCE = new RatingsRepository(daoSession);
        }
        return INSTANCE;
    }

    /**
     * Stores players in the database. If the Player exists in the database we update it otherwise
     * we insert it
     * @param allPlayers
     * @param callback to notify that the players have been stored in the database.
     */
    @Override
    public void storePlayers(Map<String, Player> allPlayers, StoreDataCallback callback) {
        PlayerDao playerDao = mDaoSession.getPlayerDao();
        List<Player> playersInDatabase = getPlayers();

        for (String key : allPlayers.keySet()) {
            if (playersInDatabase.contains(allPlayers.get(key))){
                playerDao.update(allPlayers.get(key));
            } else {
                playerDao.insert(allPlayers.get(key));
            }
        }

        callback.onDataStored();
    }

    /**
     * Gets all players from the database
     * @return List of all players
     */
    @Override
    public List<Player> getPlayers() {
        PlayerDao playerDao = mDaoSession.getPlayerDao();

        Query<Player> expenseQuery = playerDao.queryBuilder().build();
        return expenseQuery.list();
    }

    @Override
    public void cleanDatabase() {
        PlayerDao playerDao = mDaoSession.getPlayerDao();
        playerDao.deleteAll();
    }

}