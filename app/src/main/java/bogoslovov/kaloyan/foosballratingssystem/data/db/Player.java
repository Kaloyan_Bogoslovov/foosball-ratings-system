package bogoslovov.kaloyan.foosballratingssystem.data.db;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * The player database table
 */
@Entity
public class Player {

    @Id(autoincrement = true)
    private Long id;

    private String name;
    private int gamesPlayed;
    private int gamesWon;

    @Generated(hash = 30709322)
    public Player() {
    }

    public Player(String name, int gamesPlayed, int gamesWon) {
        this.name = name;
        this.gamesPlayed = gamesPlayed;
        this.gamesWon = gamesWon;
    }

    @Generated(hash = 387421084)
    public Player(Long id, String name, int gamesPlayed, int gamesWon) {
        this.id = id;
        this.name = name;
        this.gamesPlayed = gamesPlayed;
        this.gamesWon = gamesWon;
    }

    public Long getId() {
        return this.id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getGamesPlayed() {
        return this.gamesPlayed;
    }
    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }
    public int getGamesWon() {
        return this.gamesWon;
    }
    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public void addGame(boolean isGameWon){
        if (isGameWon){
            gamesPlayed++;
            gamesWon++;
        } else {
            gamesPlayed++;
        }
    }

}
