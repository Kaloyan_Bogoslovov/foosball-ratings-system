package bogoslovov.kaloyan.foosballratingssystem.data.input;

public class GameScore {

    private String firstPlayer;
    private int firstPlayerScore;
    private String secondPlayer;
    private int secondPlayerScore;

    public GameScore() {
    }

    public GameScore(String firstPlayer, int firstPlayerScore, String secondPlayer, int secondPlayerScore) {
        this.firstPlayer = firstPlayer;
        this.firstPlayerScore = firstPlayerScore;
        this.secondPlayer = secondPlayer;
        this.secondPlayerScore = secondPlayerScore;
    }

    public String getFirstPlayer() {
        return firstPlayer;
    }

    public void setFirstPlayer(String firstPlayer) {
        this.firstPlayer = firstPlayer;
    }

    public int getFirstPlayerScore() {
        return firstPlayerScore;
    }

    public void setFirstPlayerScore(int firstPlayerScore) {
        this.firstPlayerScore = firstPlayerScore;
    }

    public String getSecondPlayer() {
        return secondPlayer;
    }

    public void setSecondPlayer(String secondPlayer) {
        this.secondPlayer = secondPlayer;
    }

    public int getSecondPlayerScore() {
        return secondPlayerScore;
    }

    public void setSecondPlayerScore(int secondPlayerScore) {
        this.secondPlayerScore = secondPlayerScore;
    }
}
