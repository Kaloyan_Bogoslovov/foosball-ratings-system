package bogoslovov.kaloyan.foosballratingssystem.data;

import java.util.List;
import java.util.Map;

import bogoslovov.kaloyan.foosballratingssystem.data.db.Player;

/**
 * Main entry point for accessing the database
 */
public interface RatingsDataSource {

    interface StoreDataCallback {

        void onDataStored();

        void onError();
    }

    void storePlayers (Map<String, Player> standings, StoreDataCallback callback);

    List<Player> getPlayers();

    void cleanDatabase();
}
