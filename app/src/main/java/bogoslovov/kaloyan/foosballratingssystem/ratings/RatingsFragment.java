package bogoslovov.kaloyan.foosballratingssystem.ratings;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import bogoslovov.kaloyan.foosballratingssystem.R;
import bogoslovov.kaloyan.foosballratingssystem.data.db.Player;
import bogoslovov.kaloyan.foosballratingssystem.utils.ActivityUtils;
import bogoslovov.kaloyan.foosballratingssystem.utils.Utils;

public class RatingsFragment extends Fragment implements RatingsContract.View{

    private static final int HISTORICAL_SCORES = 1;
    private static final int UPDATE_SCORES = 2;
    private static final String INPUT_FILE_FORMAT = "text/csv";
    private static final String TAG = RatingsFragment.class.getSimpleName();

    private RatingsContract.Presenter mPresenter;
    private RecyclerView mRecyclerView;

    public RatingsFragment() {
    }

    public static RatingsFragment newInstance() {
        return new RatingsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_ratings, container, false);
        mRecyclerView = root.findViewById(R.id.recycle_view);
        setHasOptionsMenu(true);
        mPresenter.sortByWinners();
        return root;
    }

    @Override
    public void setPresenter(RatingsContract.Presenter presenter) {
        mPresenter = Utils.checkNotNull(presenter);
    }

    /**
     * @param players the sorted list of players
     */
    @Override
    public void setRecycleView(List<Player> players) {
        if (!players.isEmpty()){
            RatingsAdapter ratingsAdapter = new RatingsAdapter(players);
            mRecyclerView.setAdapter(ratingsAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_input:
                showFileChooser(
                        getResources().getString(R.string.title_select_file),
                        HISTORICAL_SCORES
                );
                break;
            case R.id.menu_update:
                showFileChooser(
                        getResources().getString(R.string.title_select_update_file),
                        UPDATE_SCORES
                );
                break;
            case R.id.menu_retrieve:
                sendStandingsEmail();
                break;
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_ratings_activity, menu);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case HISTORICAL_SCORES:
                Uri uri;
                if (data != null) {
                    uri = data.getData();

                    try {
                        mPresenter.storeGameScores(
                                ActivityUtils.readTextFromUri(getActivity(),
                                        uri)
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "issue", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case UPDATE_SCORES:
                Uri fileUri;
                if (data != null) {
                    fileUri = data.getData();

                    try {
                        mPresenter.updateGameScores(
                                ActivityUtils.readTextFromUri(getActivity(),
                                        fileUri)
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "issue", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode,resultCode,data);
        }
    }

    private void showFileChooser(String title, int scoreType){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(INPUT_FILE_FORMAT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(
                            intent,
                            title),
                    scoreType);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(
                    getActivity(),
                    getResources().getString(R.string.msg_need_file_manager), Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void sendStandingsEmail() {
        mPresenter.loadEmailBody(getResources().getString(R.string.header_email), new EmailBodyLoadedCallback() {
            @Override
            public void onEmailLoaded(String emailBody) {
                Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                emailIntent.setType("text/plain");
                emailIntent.putExtra(
                        Intent.EXTRA_SUBJECT,
                        getResources().getString(R.string.subject_email)
                );
                emailIntent.putExtra(Intent.EXTRA_TEXT, emailBody);
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }

            @Override
            public void onEmptyDatabase() {
                Toast.makeText(
                        getActivity(),
                        getResources().getString(R.string.toast_empty_database),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

}
