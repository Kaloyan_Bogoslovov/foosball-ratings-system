package bogoslovov.kaloyan.foosballratingssystem.ratings;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import bogoslovov.kaloyan.foosballratingssystem.R;
import bogoslovov.kaloyan.foosballratingssystem.data.db.Player;

public class RatingsAdapter extends RecyclerView.Adapter<RatingsViewHolder>{

    private List<Player> mPlayers;

    public RatingsAdapter(List<Player> players){
        mPlayers = players;
    }

    @NonNull
    @Override
    public RatingsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_ratings_item, parent, false);
        return new RatingsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RatingsViewHolder holder, int position) {
        Player player = mPlayers.get(position);
        holder.playerName.setText(player.getName());
        holder.gamesWon.setText(Integer.toString(player.getGamesWon()));
        holder.gamesPlayed.setText(Integer.toString(player.getGamesPlayed()));
    }

    @Override
    public int getItemCount() {
        return mPlayers.size();
    }

}
