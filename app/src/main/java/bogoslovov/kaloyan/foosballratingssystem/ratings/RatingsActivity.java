package bogoslovov.kaloyan.foosballratingssystem.ratings;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import bogoslovov.kaloyan.foosballratingssystem.R;
import bogoslovov.kaloyan.foosballratingssystem.common.FoosballRatingsBaseApp;
import bogoslovov.kaloyan.foosballratingssystem.data.RatingsRepository;
import bogoslovov.kaloyan.foosballratingssystem.data.db.DaoSession;
import bogoslovov.kaloyan.foosballratingssystem.utils.ActivityUtils;

public class RatingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ratings);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.title_rating_activity));
        setSupportActionBar(toolbar);

        RatingsFragment ratingsFragment =
                (RatingsFragment) getFragmentManager().findFragmentById(R.id.contentFrame);

        if (ratingsFragment == null) {
            ratingsFragment = RatingsFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getFragmentManager(), ratingsFragment, R.id.contentFrame);
        }

        DaoSession daoSession = ((FoosballRatingsBaseApp) getApplication()).getDaoSession();

         new RatingsPresenter(RatingsRepository.getInstance(daoSession), ratingsFragment);
    }


}
