package bogoslovov.kaloyan.foosballratingssystem.ratings;

import java.util.List;

import bogoslovov.kaloyan.foosballratingssystem.BaseView;
import bogoslovov.kaloyan.foosballratingssystem.data.db.Player;
import bogoslovov.kaloyan.foosballratingssystem.data.input.GameScore;

/**
 * Specifies the contract between the view and the presenter
 */
public interface RatingsContract {

    interface View extends BaseView<Presenter> {
        void setRecycleView(List<Player> players);

        interface EmailBodyLoadedCallback {

            void onEmailLoaded(String emailBody);

            void onEmptyDatabase();
        }
    }

    interface Presenter {

        void storeGameScores(List<GameScore> gameScoreList);

        void updateGameScores(List<GameScore> gameScoreList);

        void sortByWinners();

        void loadEmailBody(String header, View.EmailBodyLoadedCallback callback);
    }
}