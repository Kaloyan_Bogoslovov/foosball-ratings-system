package bogoslovov.kaloyan.foosballratingssystem.ratings;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bogoslovov.kaloyan.foosballratingssystem.data.RatingsDataSource;
import bogoslovov.kaloyan.foosballratingssystem.data.db.Player;
import bogoslovov.kaloyan.foosballratingssystem.data.input.GameScore;
import bogoslovov.kaloyan.foosballratingssystem.utils.Utils;

/**
 *  Executes all the logic that doesn't depend on Android classes.
 */
public class RatingsPresenter implements RatingsContract.Presenter{

    private static final String TAG = RatingsPresenter.class.getSimpleName();

    private final RatingsDataSource mRatingsRepository;
    private final RatingsContract.View mRatingsView;

    /**
     *
     * @param ratingsRepository the repository contains all methods that access the database
     * @param ratingsView the ratings view
     */
    public RatingsPresenter(RatingsDataSource ratingsRepository, RatingsContract.View ratingsView) {
        mRatingsRepository = Utils.checkNotNull(ratingsRepository, "ratingsRepository cannot be null");
        mRatingsView = Utils.checkNotNull(ratingsView,"ratingsView cannot be null");

        mRatingsView.setPresenter(this);
    }

    /**
     * Gets all of the players sorts them and sends the List to the UI
     */
    @Override
    public void sortByWinners() {
        List<Player> players = mRatingsRepository.getPlayers();
        sortArrayByWinners(players);
        mRatingsView.setRecycleView(players);
    }

    /**
     *  Gets all of the players from the db, sorts them, and build the email body.
     *  Sends the email body via the callback.
     * @param header The first line of the email
     * @param callback used for sending the email body,
     *                or if the db is empty to notify that there are no records in the db
     */
    @Override
    public void loadEmailBody(String header, RatingsContract.View.EmailBodyLoadedCallback callback) {
        List<Player> players = mRatingsRepository.getPlayers();

        if (!players.isEmpty()){
            sortArrayByWinners(players);

            StringBuilder emailBody = new StringBuilder();
            emailBody.append("\n");
            emailBody.append(header);

            for (Player player : players) {
                emailBody.append(player.getName());
                emailBody.append(", ");
                emailBody.append(player.getGamesPlayed());
                emailBody.append(", ");
                emailBody.append(player.getGamesWon());
                emailBody.append("\n");
            }
            callback.onEmailLoaded(emailBody.toString());
        } else {
            callback.onEmptyDatabase();
        }
    }

    /**
     * Takes the list of scores from the csv file, converts it to the current standings
     * and stores them in the database.
     * @param gameScoreList the List of game scores acquired from the csv file
     */
    @Override
    public void storeGameScores(List<GameScore> gameScoreList) {
        List<Player> playerList = mRatingsRepository.getPlayers();
        Map<String, Player> standings = new HashMap<String,Player>();

        for (Player player : playerList) {
            standings.put(player.getName(), player);
        }

        storeScores(gameScoreList, standings);
    }

    /**
     * Cleans the database, takes the list of scores from the csv file, converts it to the current standings
     * and stores them in the database.
     * @param gameScoreList the List of game scores acquired from the csv file
     */
    @Override
    public void updateGameScores(List<GameScore> gameScoreList) {
        mRatingsRepository.cleanDatabase();
        Map<String, Player> standings = new HashMap<String,Player>();
        storeScores(gameScoreList, standings);
    }

    private void storeScores(List<GameScore> gameScoreList, Map<String, Player> standings){
        for (GameScore gameScore : gameScoreList) {
            if (isFirstPlayerWinner(gameScore)){
                addFirstPlayerAsWinner(standings, gameScore);
            } else {
                addSecondPlayerAsWinner(standings, gameScore);
            }
        }

        mRatingsRepository.storePlayers(standings, new RatingsDataSource.StoreDataCallback() {
            @Override
            public void onDataStored() {
                List<Player> players = mRatingsRepository.getPlayers();
                sortArrayByWinners(players);
                mRatingsView.setRecycleView(players);
            }

            @Override
            public void onError() {
                //todo finish the error handling
            }
        });
    }

    private void sortArrayByWinners(List<Player> players){
        if (!players.isEmpty()){
            Collections.sort(players, new Comparator<Player>() {
                @Override
                public int compare(Player playerOne, Player playerTwo) {
                    return Integer.compare(playerTwo.getGamesWon(), playerOne.getGamesWon());
                }
            });
        }
    }

    private boolean isFirstPlayerWinner(GameScore gameScore){
        return gameScore.getFirstPlayerScore() > gameScore.getSecondPlayerScore();
    }

    private void addFirstPlayerAsWinner(Map<String, Player> standings, GameScore gameScore){
        addWinningPlayer(standings, gameScore.getFirstPlayer());
        addLosingPlayer(standings, gameScore.getSecondPlayer());
    }

    private void addSecondPlayerAsWinner(Map<String, Player> standings, GameScore gameScore){
        addWinningPlayer(standings, gameScore.getSecondPlayer());
        addLosingPlayer(standings, gameScore.getFirstPlayer());
    }

    private void addWinningPlayer(Map<String, Player> standings, String player){
        if (standings.containsKey(player)){
            standings.get(player).addGame(true);
        } else {
            standings.put(player, new Player(player,1,1));
        }
    }

    private void addLosingPlayer(Map<String, Player> standings, String player){
        if (standings.containsKey(player)){
            standings.get(player).addGame(false);
        } else {
            standings.put(player, new Player(player,1,0));
        }
    }

}
