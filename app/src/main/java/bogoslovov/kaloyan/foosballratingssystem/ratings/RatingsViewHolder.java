package bogoslovov.kaloyan.foosballratingssystem.ratings;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import bogoslovov.kaloyan.foosballratingssystem.R;

public class RatingsViewHolder extends RecyclerView.ViewHolder{

    public TextView playerName;
    public TextView gamesPlayed;
    public TextView gamesWon;


    public RatingsViewHolder(View view) {
        super(view);

        playerName = view.findViewById(R.id.text_player_name);
        gamesPlayed = view.findViewById(R.id.text_games_played);
        gamesWon = view.findViewById(R.id.text_games_won);
    }
}
