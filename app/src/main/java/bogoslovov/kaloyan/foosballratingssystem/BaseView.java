package bogoslovov.kaloyan.foosballratingssystem;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
